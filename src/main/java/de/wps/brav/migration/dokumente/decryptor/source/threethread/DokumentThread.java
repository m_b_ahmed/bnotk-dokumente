package de.wps.brav.migration.dokumente.decryptor.source.threethread;

import java.beans.PropertyVetoException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.wps.brav.migration.dokumente.PropertiesLoader;
import de.wps.brav.migration.dokumente.db.DataSource;

public class DokumentThread {

	private static Connection sourceConnection = null;
	private static Connection targetConnection = null;
	private static boolean detailedLogsEnabled = true;
	private static DateFormat dfOut = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private static Properties props;
//	private static ExecutorService startProcess = Executors.newFixedThreadPool(2);
	//private static int sizeOfTheReadingChunk = 50;
	private static String selectTotalDokCountQuery = null;
	private static boolean rerunable = false;
 	private static PrintStream ps = null;
 	Date startTime = new Date();
 	int totalRecordsProcessed = 0;
 	int packageCount =1;
 	private static int startDBReadIndex= 0;
 	private static int recordSize = 1000;
	private static void close() {
	//	startProcess.shutdown();
		try {
			//System.out.println(" CLosing  DB connections");
			if (targetConnection != null) {
				//System.out.println(" CLosing connection target");
				targetConnection.close();
			}
			if (sourceConnection != null) {
			//	System.out.println(" CLosing connection source");
				sourceConnection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inizialisiert das Properties-Object aus der Properties-datei
	 */
	private static void initialisiere(String[] args) {
		String propertiesfilePathName = args[0];
		System.out.println(dfOut.format(new Date()) + " > " + " INFO  Properties Datei " + propertiesfilePathName
				+ " wird geladen");
		props = PropertiesLoader.loadProperties(propertiesfilePathName);
		System.out.println(dfOut.format(new Date()) + " > " + " INFO  Properties Datei " + propertiesfilePathName
				+ " wurde erfolgreich geladen");
	}

	private static void doInitialSettings() {
		rerunable = "true".equalsIgnoreCase(props.getProperty("rerunable"));
		if (rerunable) {
			selectTotalDokCountQuery = props.getProperty("selectTotalDokCountQuery_status_column");
		} else {
			selectTotalDokCountQuery = props.getProperty("selectTotalDokCountQuery");
		}
		detailedLogsEnabled = "1".equalsIgnoreCase(props.getProperty("EnableDetailedLogs"));
		recordSize =(props.getProperty("packetSize")!=null && props.getProperty("packetSize").length()!=0 )?Integer.parseInt(props.getProperty("packetSize")):1000;
		DataSource.setProps(props);
		/** Initial settings - end */
	}
	
	private static int getDokCount() {
//		if (detailedLogsEnabled)
//		ps.println(dfOut.format(new Date()) + " class DokumenteChunkReadThreeThreadExecutors method getdokcount in");
		int count = 0;
		try (Connection sourceConnection = DataSource.getInstance().getSourceConnection();
				PreparedStatement statement = sourceConnection.prepareStatement(selectTotalDokCountQuery);) {

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				count = rs.getInt("TOTAL_COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace(ps);
		} catch (IOException e) {
			e.printStackTrace(ps);
		} catch (PropertyVetoException e) {
			e.printStackTrace(ps);
		}
//		if (detailedLogsEnabled)
//		ps.println(dfOut.format(new Date()) + " class DokumenteChunkReadThreeThreadExecutors method getdokcount total record count--"+count);
		return count;
	}
	public static void main(String[] args) {
		ps = System.out;
		try {
			if (args != null && args.length >0) {
				initialisiere(args);
				ps.println(dfOut.format(new Date())
						+ " > Entering the record processing DokumenteChunkReadThreeThreadExecutors");
				
				String logFileLocation = props.getProperty("LogFileLocation");
				String logFileLocatiionSuffix = "0";
				if(args[1]!=null && args[1].trim().length()>0)
					logFileLocatiionSuffix = args[1];
				try {
					startDBReadIndex= Integer.parseInt(logFileLocatiionSuffix);
					logFileLocatiionSuffix = "_"+logFileLocatiionSuffix+".";
					logFileLocation = logFileLocation.replace(".",logFileLocatiionSuffix);
					ps = new PrintStream(new FileOutputStream(logFileLocation));
				} catch (Exception e) {
					// e.printStackTrace();
					ps.println("Error in getting logfile location : " + logFileLocation
							+ " All logs will be written to console only");
				}
//				ps.println(dfOut.format(new Date()) + " > INFO  START "
//						+ DokumenteChunkReadThreeThreadExecutors.class.getName());
//				if ("1".equals(props.getProperty("FromDatabaseParallelDecrypt"))) {
				doInitialSettings();
				//entryForProcessing();
				DokumentThread obj = new DokumentThread();
				obj.startRecordProcessing(args[0]);
//				}
			
			} else {
				System.out.println(dfOut.format(new Date())
						+ " > Please enter only one parameter with the full path and name of the properties file");
			}
		} finally {
			close();
			System.out.println(dfOut.format(new Date()) + " > INFO  END DokumenteChunkReadThreeThreadExecutors");
		}

	}
	
   private  void startRecordProcessing(String propFileLocation) {
//	   if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " startRecordProcessing==>");
	    DokumentProcessor dp = new DokumentProcessor(ps,props);
		int totalMeldCountActual = getDokCount(); 
		int totalMeldCountActualTemp = totalMeldCountActual; 
		
		int recordProcessing = totalMeldCountActual<=1000?totalMeldCountActual:1000;
		
		Date endTime = null;
		int startIndex = 0;
		int endIndex = 0;
		boolean done = true;
		Date processStartTime = null;
		
		
		int recordIteration = 0;
		boolean continueProcess = true;
		int failedcount = 0;
		
		int count =0;
//		if (detailedLogsEnabled)
//			ps.println(dfOut.format(new Date()) + " startRecordProcessing==> for the total record count --- ");
			if(totalMeldCountActual==0) {
				continueProcess = false;
				dp.stopThreads();
				close();
				
			}
				
			
			while(continueProcess && failedcount<6) {
				try {
					dp.initializeStatisticMap();
				
					processStartTime = new Date();
//					if (detailedLogsEnabled)
//					ps.println(dfOut.format(new Date()) + " continueProcess== "  +continueProcess + " count == " +count
//							+ " Process Records  == " + recordProcessing );
					if((count==0 && totalMeldCountActual<1000) || count>0 ) {
						continueProcess = false;
					}
//					recordIteration = (recordProcessing / recordSize)
//							+ ((recordProcessing % recordSize) > 0 ? 1 : 0);
					 done =dp.startProcessing(startDBReadIndex, startDBReadIndex+recordSize,recordSize);
					 if(!done) {
						 failedcount++;
						 if(failedcount>5)
							 ps.println("Failed Record fetch "+failedcount);
					 }
//					for(int i =0;i<recordIteration;i++) {
//						startIndex = i*recordSize;
//						endIndex  = startIndex+recordSize;
//						if(endIndex >recordProcessing)
//							endIndex = recordProcessing;
//						 done =dp.startProcessing(startIndex+startDBReadIndex, endIndex+startDBReadIndex,recordProcessing);
//						 if(!done) {
//							 failedcount++;
//							 if(failedcount>5)
//								 ps.println("Failed Record fetch "+failedcount);
//						 }
//						
//						// totalRecordsProcessed = totalRecordsProcessed+(endIndex-startIndex);
//					}
					 totalRecordsProcessed = totalRecordsProcessed+recordProcessing;
				// Again 10000 records to process
				HashMap<String,Double> statMap = dp.getStatisticMap();
					endTime = new Date();
					ps.println("statMap-after---"+statMap);
					long milli = (endTime.getTime() - startTime.getTime());
					long milliProcess = (endTime.getTime() - processStartTime.getTime());

					double totalTimeInMin = (((double) milli / 1000) / 60);
					double totalTimeInMinProcess = (((double) milliProcess / 1000) / 60);
					double averageSpeed = ((double) totalMeldCountActualTemp / totalTimeInMin);
					
						ps.println(dfOut.format(new Date()) + ": Total Time taken:%.3f |"+totalTimeInMin+" |Number of total records Processed| "+totalRecordsProcessed+
								"For package :"  +packageCount
								+ "-DB Read Time :  %.3f Records/Minutes : " +(recordProcessing/(statMap.get("DBREAD"))
								+ "-Data Processing : %.3f Records/Minutes :" +(recordProcessing/statMap.get("RECPROCSTIME"))+
								"-Communication with SDS : %.3f Records/Minutes :"+(recordProcessing/statMap.get("SDSTIME"))+
								"-Transformation Data Update :  %.3f Records/Minutes :"+(recordProcessing/statMap.get("DBWRITE"))+
								"Time for this package :  %.3f :"+totalTimeInMinProcess));
						ps.println("");
						ps.printf("STATISTICSENTRY |"+dfOut.format(new Date()) + "| Total Time taken | %.3f  |Number of total records Processed| %d"+
								" |For package | %d"  
								+ "|DB Read Time |  %.3f Records/Minutes | " 
								+ "Data Processing | %.3f Records/Minutes |" +
								" Communication with SDS | %.3f Records/Minutes "+
								"|Transformation Data Update |  %.3f Records/Minutes "+
								"|Time for this package |  %.3f | ",totalTimeInMin,totalRecordsProcessed,packageCount,(recordProcessing/statMap.get("DBREAD")),
								(recordProcessing/statMap.get("RECPROCSTIME")),(recordProcessing/statMap.get("SDSTIME")),(recordProcessing/statMap.get("DBWRITE")),
								totalTimeInMinProcess);
						ps.println("");
						if(totalMeldCountActual<=recordProcessing) {
						recordProcessing = totalMeldCountActual;
						count++;
						packageCount++;
						}else {
						totalMeldCountActual = totalMeldCountActual - recordProcessing;
						packageCount++;
						
						}
////				if (detailedLogsEnabled)
////					ps.println(dfOut.format(new Date()) + " end of while continueProcess== "  +continueProcess
////							+ " totalMeldCountActual == " + totalMeldCountActual + " count == " +count);
//						// ps.println(" b4 thread stop-- ----"); 
//						try {
////					 if (detailedLogsEnabled)
////							ps.println(dfOut.format(new Date()) + " Calling thread sleep to avaoid Memmory error estimated to stop the thtreads 2 mins and forcing Garbage collections");
//							// ps.println(" b4 thread sleep-- ----"); 
////						Thread.sleep(1000*1);
////						dp.stopThreads();
//						// ps.println(" Aft thread sleep-- -and stop---"); 
//						 System.gc();
//						}catch (Exception e) {
//							// TODO: handle exception
//							// ps.println(" Exception--in try inner ----"+e.getMessage()); 
//							 e.printStackTrace();
//						} 
				}catch (Exception e) {
					// TODO: handle exception
					 dp.stopThreadsForce();
					// continueProcess = true;
					 System.gc();
					// ps.println(" Exception------"+e.getMessage()); 
					 e.printStackTrace();
					 startRecordProcessing(propFileLocation);
				}
			}
			// ps.println(" Outside while conprocc--in try inner ----"+continueProcess); 
			
			dp.stopThreads();
			close();
		
//		if (detailedLogsEnabled)
//			ps.println(dfOut.format(new Date()) + " > Number Of Records To Be Read From Meldungen == "  +totalMeldCountActual
//					+ " sizeOfTheReadingChunk == " + sizeOfTheReadingChunk + " listVO == " );
		endTime = new Date();
		
		long milli = (endTime.getTime() - startTime.getTime());

		double totalTimeInMin = (((double) milli / 1000) / 60);
		double averageSpeed = ((double) totalMeldCountActualTemp / totalTimeInMin);
		
		ps.printf(dfOut.format(new Date()) + " > STATISTICSENTRY"
				+ "\n Final Total Records from DB %d |Total record  Processed : %d | Total Time Elapsed : %.3f Minutes | Average Speed : %.3f Records/Min."
				+ "\n", totalMeldCountActualTemp,totalRecordsProcessed, totalTimeInMin, averageSpeed);
   }
}
