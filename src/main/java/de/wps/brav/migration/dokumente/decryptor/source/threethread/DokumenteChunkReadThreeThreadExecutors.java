package de.wps.brav.migration.dokumente.decryptor.source.threethread;

import java.beans.PropertyVetoException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.bnotk.zvr.common.rest.client.AuthenticationHeader;
import de.bnotk.zvr.interfaces.sds.storedocument.SdsEncoding;
import de.wps.brav.migration.dokumente.PropertiesLoader;
import de.wps.brav.migration.dokumente.db.DataSource;
import de.wps.brav.migration.dokumente.db.DokumenteVO;
import de.wps.brav.migration.dokumente.db.TMPDokumenteVO;
import de.wps.brav.migration.dokumente.request.Attributes;
import de.wps.brav.migration.dokumente.request.Content;
import de.wps.brav.migration.dokumente.request.DocumentDetails;
import de.wps.brav.migration.dokumente.request.StoreDocumentRequest;

public class DokumenteChunkReadThreeThreadExecutors {

	private static Connection sourceConnection = null;
	private static Connection targetConnection = null;
	private static boolean detailedLogsEnabled = true;
	private static DateFormat dfOut = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private static Properties props;
	@SuppressWarnings("unused")
	private static String selectTotalDokCountQuery = null;
	private static boolean rerunable = false;
 	private static PrintStream ps = null;
 	@SuppressWarnings("unused")
	private static int sizeOfTheReadingChunk = 50;
 	@SuppressWarnings("unused")
	private static int sizeOfTheSendRestRequestChunk = 50;
 	@SuppressWarnings("unused")
	private static int sizeOfTheWritingChunk = 25;
 	@SuppressWarnings("unused")
	private static int numberOfRecordsReadFromDB = 100;
 	@SuppressWarnings("unused")
	private static int readAndProcessExecutorThreadPoolSize = 40;
	private static String updateSDSIDPreparedStatementSQL = null;
	private static String updateSDSIDStatusPreparedStatementSQL = null;
	private static String selectCryptDataAndIdPagingQuery = null;
	@SuppressWarnings("unused")
	private static String selectDoktypeVmData = null;

	private static String sdsRestServerURL = null;

	private static String sdsRestServerApp = null;
	private static String sdsRestServerUser = null;
	private static String sdsRestServerPassword = null;
	
	//von Ugur eingef�gt
	private static String OwnerOfDocument = null;
	private static String sdsGroup = null;
	
	
	
	
	
	private HashMap<String,Double> statisticMap= new HashMap<String,Double>();
 	private static PrintStream psError = null;
 	Date startTime = new Date();
 	int totalRecordsProcessed = 0;
 	int packageCount =1;
 	private static int startDBReadIndex= 0;
 	private static int recordSize = 1000;
 
	private static void close() {
	//	startProcess.shutdown();
		try {
			//System.out.println(" CLosing  DB connections");
			if (targetConnection != null) {
				//System.out.println(" CLosing connection target");
				targetConnection.close();
			}
			if (sourceConnection != null) {
			//	System.out.println(" CLosing connection source");
				sourceConnection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inizialisiert das Properties-Object aus der Properties-datei
	 */
	private static void initialisiere(String[] args) {
		String propertiesfilePathName = args[0];
		System.out.println(dfOut.format(new Date()) + " > " + " INFO  Properties Datei " + propertiesfilePathName
				+ " wird geladen");
		System.out.println("�nderung");
		props = PropertiesLoader.loadProperties(propertiesfilePathName);
		System.out.println(dfOut.format(new Date()) + " > " + " INFO  Properties Datei " + propertiesfilePathName
				+ " wurde erfolgreich geladen");
	}

	private static void doInitialSettings() {
		rerunable = "true".equalsIgnoreCase(props.getProperty("rerunable"));
		if (rerunable) {
			selectTotalDokCountQuery = props.getProperty("selectTotalDokCountQuery_status_column");
		} else {
			selectTotalDokCountQuery = props.getProperty("selectTotalDokCountQuery");
		}
		detailedLogsEnabled = "1".equalsIgnoreCase(props.getProperty("EnableDetailedLogs"));
		recordSize =(props.getProperty("packetSize")!=null && props.getProperty("packetSize").length()!=0 )?Integer.parseInt(props.getProperty("packetSize")):1000;

		readAndProcessExecutorThreadPoolSize = Integer
				.parseInt(props.getProperty("ReadAndProcessExecutorsThreadPoolSize"));

		sizeOfTheReadingChunk = Integer.parseInt(props.getProperty("sizeOfTheReadingChunkFromDokumente"));

		sizeOfTheSendRestRequestChunk = Integer.parseInt(props.getProperty("sizeOfTheSendRestRequestChunk"));

		sizeOfTheWritingChunk = Integer.parseInt(props.getProperty("sizeOfTheWritingChunkIntoDokumente"));

		numberOfRecordsReadFromDB = Integer.parseInt(props.getProperty("NumberOfRecordsToBeReadFromDokumente"));

		if (rerunable) {
			selectCryptDataAndIdPagingQuery = props.getProperty("SelectCryptDataAndIdPagingQuery_status_column");
			selectTotalDokCountQuery = props.getProperty("selectTotalDokCountQuery_status_column");
			updateSDSIDPreparedStatementSQL = props.getProperty("updateSDSIDQuery_status_column");
		} else {
			selectCryptDataAndIdPagingQuery = props.getProperty("SelectCryptDataAndIdPagingQuery");
			selectTotalDokCountQuery = props.getProperty("selectTotalDokCountQuery");
			updateSDSIDPreparedStatementSQL = props.getProperty("updateSDSIDQuery");
		}

		updateSDSIDStatusPreparedStatementSQL = props
				.getProperty("updateSDSIDStatusPreparedStatementSQL_status_column");

		sdsRestServerURL = props.getProperty("SDSRestServerURL");

		sdsRestServerApp = props.getProperty("SDSRestServerApp");

		sdsRestServerUser = props.getProperty("SDSRestServerUser");

		sdsRestServerPassword = props.getProperty("SDSRestServerPassword");

		selectDoktypeVmData = props.getProperty("selectDoktypeVmData");

		detailedLogsEnabled = "1".equalsIgnoreCase(props.getProperty("EnableDetailedLogs"));
		
		//von Ugur eingef�gt
		OwnerOfDocument = props.getProperty("OwnerOfDocument");
		sdsGroup = props.getProperty("SDSRestGroup");
	


		
		DataSource.setProps(props);
		/** Initial settings - end */
	}
	
//	private static int getDokCount() {
////		if (detailedLogsEnabled)
////		ps.println(dfOut.format(new Date()) + " class DokumenteChunkReadThreeThreadExecutors method getdokcount in");
//		int count = 0;
//		try (Connection sourceConnection = DataSource.getInstance().getSourceConnection();
//				PreparedStatement statement = sourceConnection.prepareStatement(selectTotalDokCountQuery);) {
//
//			ResultSet rs = statement.executeQuery();
//			if (rs.next()) {
//				count = rs.getInt("TOTAL_COUNT");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace(ps);
//		} catch (IOException e) {
//			e.printStackTrace(ps);
//		} catch (PropertyVetoException e) {
//			e.printStackTrace(ps);
//		}
////		if (detailedLogsEnabled)
////		ps.println(dfOut.format(new Date()) + " class DokumenteChunkReadThreeThreadExecutors method getdokcount total record count--"+count);
//		return count;
//	}
	public static void main(String[] args) {
		ps = System.out;
		try {
			if (args != null && args.length >0) {
				initialisiere(args);
				ps.println(dfOut.format(new Date())
						+ " > Entering the record processing DokumenteChunkReadThreeThreadExecutors");
				
				String logFileLocation = props.getProperty("LogFileLocation");
				String logFileLocatiionSuffix = "0";
			//	if(args[1]!=null && args[1].trim().length()>0)
			//		logFileLocatiionSuffix = args[1];
				try {
					startDBReadIndex= Integer.parseInt(logFileLocatiionSuffix);
					logFileLocatiionSuffix = "_"+logFileLocatiionSuffix+".";
					logFileLocation = logFileLocation.replace(".",logFileLocatiionSuffix);
					ps = new PrintStream(new FileOutputStream(logFileLocation));
					psError = new PrintStream(new FileOutputStream(logFileLocation.replace(".", "_Error.")));
				} catch (Exception e) {
					// e.printStackTrace();
					psError.println("Error in getting logfile location : " + logFileLocation
							+ " All logs will be written to console only");
				}
//				ps.println(dfOut.format(new Date()) + " > INFO  START "
//						+ DokumenteChunkReadThreeThreadExecutors.class.getName());
//				if ("1".equals(props.getProperty("FromDatabaseParallelDecrypt"))) {
				doInitialSettings();
				//entryForProcessing();
				DokumenteChunkReadThreeThreadExecutors obj = new DokumenteChunkReadThreeThreadExecutors();
				obj.startRecordProcessing();
//				}
			
				System.out.println(dfOut.format(new Date())
						+ " > Please enter atleast one parameter with the full path and name of the properties file");
			}
		} finally {
			close();
			System.out.println(dfOut.format(new Date()) + " > INFO  END DokumenteChunkReadThreeThreadExecutors");
		}

	}
	
   private  void startRecordProcessing() {
	   /*
	    * 1. Read Dta  from DB 
	    */
//	   if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " startRecordProcessing==>");
		@SuppressWarnings("unused")
		Date endTime = null;
		@SuppressWarnings("unused")
		Date processStartTime = null;
		//int recordIteration = 0;
		boolean continueProcess = true;
		int failedcount = 0;
		List<DokumenteVO> listVO  = null;
	//	int count =0;
			while(continueProcess && failedcount<1) {
				try {
					processStartTime = new Date();
					statisticMap.clear();
					listVO  = readDBData(selectCryptDataAndIdPagingQuery,startDBReadIndex,startDBReadIndex+recordSize);
					if(listVO.size()>0) {
						processAndSDSUpload(listVO);
					}else {
						failedcount++;
					}
					endTime = new Date();
				//	long milli = (endTime.getTime() - startTime.getTime());
				//	long milliProcess = (endTime.getTime() - processStartTime.getTime());

				//	double totalTimeInMin = (((double) milli / 1000) / 60);
				//	double totalTimeInMinProcess = (((double) milliProcess / 1000) / 60);
				//	double averageSpeed = ((double) recordSize / totalTimeInMin);
					/*
						ps.println(dfOut.format(new Date()) + ": Total Time taken:%.3f |"+totalTimeInMin+" |Number of total records Processed| "+totalRecordsProcessed+
								"For package :"  +packageCount
								+ "-DB Read Time :  %.3f Records/Minutes : " +(recordProcessing/(statMap.get("DBREAD"))
								+ "-Data Processing : %.3f Records/Minutes :" +(recordProcessing/statMap.get("RECPROCSTIME"))+
								"-Communication with SDS : %.3f Records/Minutes :"+(recordProcessing/statMap.get("SDSTIME"))+
								"-Transformation Data Update :  %.3f Records/Minutes :"+(recordProcessing/statMap.get("DBWRITE"))+
								"Time for this package :  %.3f :"+totalTimeInMinProcess));
						ps.println("");
						ps.printf("STATISTICSENTRY |"+dfOut.format(new Date()) + "| Total Time taken | %.3f  |Number of total records Processed| %d"+
								" |For package | %d"  
								+ "|DB Read Time |  %.3f Records/Minutes | " 
								+ "Data Processing | %.3f Records/Minutes |" +
								" Communication with SDS | %.3f Records/Minutes "+
								"|Transformation Data Update |  %.3f Records/Minutes "+
								"|Time for this package |  %.3f | ",totalTimeInMin,totalRecordsProcessed,packageCount,(recordProcessing/statMap.get("DBREAD")),
								(recordProcessing/statMap.get("RECPROCSTIME")),(recordProcessing/statMap.get("SDSTIME")),(recordProcessing/statMap.get("DBWRITE")),
								totalTimeInMinProcess);
						ps.println("");*/
						
////				if (detailedLogsEnabled)
////					ps.println(dfOut.format(new Date()) + " end of while continueProcess== "  +continueProcess
////							+ " totalMeldCountActual == " + totalMeldCountActual + " count == " +count);
//						// ps.println(" b4 thread stop-- ----"); 
//						try {
////					 if (detailedLogsEnabled)
////							ps.println(dfOut.format(new Date()) + " Calling thread sleep to avaoid Memmory error estimated to stop the thtreads 2 mins and forcing Garbage collections");
//							// ps.println(" b4 thread sleep-- ----"); 
////						Thread.sleep(1000*1);
////						dp.stopThreads();
//						// ps.println(" Aft thread sleep-- -and stop---"); 
//						 System.gc();
//						}catch (Exception e) {

//							// ps.println(" Exception--in try inner ----"+e.getMessage()); 
//							 e.printStackTrace();
//						} 
				}catch (Exception e) {
					
					// dp.stopThreadsForce();
					// continueProcess = true;
					 System.gc();
					// ps.println(" Exception------"+e.getMessage()); 
					 e.printStackTrace();
				
				}
			}
			// ps.println(" Outside while conprocc--in try inner ----"+continueProcess); 
			
		
		
//		if (detailedLogsEnabled)
//			ps.println(dfOut.format(new Date()) + " > Number Of Records To Be Read From Meldungen == "  +totalMeldCountActual
//					+ " sizeOfTheReadingChunk == " + sizeOfTheReadingChunk + " listVO == " );
		endTime = new Date();
		
		//long milli = (endTime.getTime() - startTime.getTime());

		//double totalTimeInMin = (((double) milli / 1000) / 60);
		//double averageSpeed = ((double) 78888 / totalTimeInMin);
//		
//		ps.printf(dfOut.format(new Date()) + " > STATISTICSENTRY"
//				+ "\n Final Total Records from DB %d |Total record  Processed : %d | Total Time Elapsed : %.3f Minutes | Average Speed : %.3f Records/Min."
//				+ "\n", totalMeldCountActualTemp,totalRecordsProcessed, totalTimeInMin, averageSpeed);
   }
   private  List<DokumenteVO> readDBData (String selectCryptDataAndIdPagingQuery,int startIndex,int lastIndex) {
		List<DokumenteVO> listDokumenteVO = new ArrayList<DokumenteVO>();
		Connection sourceConnection = null;
		try  {
			 sourceConnection = DataSource.getInstance().getSourceConnection();
			 PreparedStatement statement = sourceConnection.prepareStatement(selectCryptDataAndIdPagingQuery);
			
			statement.setInt(1, lastIndex);
			statement.setInt(2, startIndex);
			ResultSet rs = statement.executeQuery();
			statement.setFetchSize(lastIndex-startIndex);
			while (rs.next()) {
				String dokid = rs.getString("DOKID");
				BigDecimal doktyp = rs.getBigDecimal("DOKTYP");
				Date cretms = rs.getDate("CRETMS");
				Date drucktms = rs.getDate("DRUCKTMS");
				boolean alfrescokz = rs.getBoolean("ALFRESCOKZ");
				Date gelesentms = rs.getDate("GELESENTMS");
				String meldid = rs.getString("MELDID");
				String vmid = rs.getString("VMID");
				BigDecimal anzseiten = rs.getBigDecimal("ANZSEITEN");
				BigDecimal bevollmnr = rs.getBigDecimal("BEVOLLMNR");
				String dokbez = rs.getString("DOKBEZ");
				Blob dokdata = rs.getBlob("DOKDATA");
				String landkz = rs.getString("LANDKZ");
				boolean status = false;
				if (rerunable) {
					status = rs.getBoolean("STATUS");
				}
				//String dokumentart = rs.getString("DOKUMENTART");
				String dokumentName = rs.getString("DOKNAME");
				String dokArtbez = rs.getString("DOKUMENTARTBEZ");
				dokumentName =dokumentName != null ? dokumentName : "-";

				DokumenteVO objDokumenteVO = new DokumenteVO(dokid, doktyp, cretms, drucktms, alfrescokz, gelesentms,
						meldid, vmid, anzseiten, bevollmnr, dokbez, dokdata, landkz);
				objDokumenteVO.setDokArtbez(dokArtbez);
				objDokumenteVO.setStatus(status);
				dokumentName = dokumentName.replaceAll("[$&+,:;=?@#|/'\\\\<>^*()%]", "-");
				objDokumenteVO.setDokName(dokumentName);
				byte[] blobByteArray = null;
					if (dokdata != null) {
						blobByteArray = dokdata.getBytes((long) 1, (int) (dokdata.length()));
					}
				objDokumenteVO.setCryptDataByteArray(blobByteArray);

				listDokumenteVO.add(objDokumenteVO);

			}
		//	if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > Reading data from offset == " + startIndex + " range == " + lastIndex
						+ "Total records found :- " + listDokumenteVO.size());

		} catch (SQLException e) {
			e.printStackTrace(ps);
		} catch (IOException e) {
			e.printStackTrace(ps);
		} catch (PropertyVetoException e) {
			e.printStackTrace(ps);
		}finally {
			try {
				sourceConnection.close();
			}catch (Exception e) {
				
			}
			
		}
		if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " >Data frim dbreader sent to call function "  + listDokumenteVO.size());
		return listDokumenteVO;
	}
   private boolean processAndSDSUpload(List<DokumenteVO> dblist) {
		
		if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " class DKProcessor method processandwrite in");
		boolean completed = false;

		//Date readingStartTime = new Date();
		Date processingStartTime = new Date();

		List<DokumenteVO> listDokumenteVO = new ArrayList<DokumenteVO>();

		for (DokumenteVO mv : dblist) {
			DokumenteVO mvWithStream = process(mv);
			listDokumenteVO.add(mvWithStream);
		}
		Date processingEndTime = new Date();
		
		 long milli = (processingEndTime.getTime() - processingStartTime.getTime());

		double totalTimeInMin = (((double) milli / 1000) / 60);
		
		setStatisticMap("RECPROCSTIME", totalTimeInMin);
		

		/** After processing the data */

		List<TMPDokumenteVO> keyList = new ArrayList<TMPDokumenteVO>();
		
		for (DokumenteVO objDokumenteVO : listDokumenteVO) {
			String accessKey = uploadDocument(objDokumenteVO);
			passedresponses.add("{" + "\"dokid\":" + "\"" + objDokumenteVO.getDokid() + "\",\"accessKey\":" + "\""
					+ accessKey + "\"" + "}");
			keyList.add(new TMPDokumenteVO(objDokumenteVO.getDokid(), accessKey));

		}
		
		completed = write(keyList);
		return completed;
	}


	
	

	private DokumenteVO process(DokumenteVO objDokumenteVO) {
		if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " class DKProcessor method process in ");
		// ByteArrayInputStream byteArrayInputStream = null;
		try {
			InputStream inputStream = new ByteArrayInputStream(objDokumenteVO.getCryptDataByteArray());
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			decryptStreamData(inputStream, byteArrayOutputStream);
			objDokumenteVO.setDecryptDataByteArray(byteArrayOutputStream.toByteArray());
			inputStream.close();
			byteArrayOutputStream.close();

		} catch (IOException e) {
		}

		return objDokumenteVO;
	}

	private void decryptStreamData(InputStream is, OutputStream os) {

		try {
			final int UNZIP_BUFFER_SIZE = 1024;
			final int DECRYPT_BUFFER_SIZE = 100;
			byte[] keyByteArray = { (byte) 0x1E, (byte) 0x54, (byte) 0x35, (byte) 0x43, (byte) 0x43, (byte) 0xF4,
					(byte) 0x8C, (byte) 0x9A };
			byte[] ivBytes = { (byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0x78, (byte) 0x90, (byte) 0xAB,
					(byte) 0xCD, (byte) 0xEF };
			DESKeySpec desks = new DESKeySpec(keyByteArray);
			SecretKey skey = null;
			skey = SecretKeyFactory.getInstance("DES").generateSecret(desks);

			IvParameterSpec ivps = new IvParameterSpec(ivBytes);
			Cipher cipher = Cipher.getInstance("DES/CBC/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, skey, ivps);

			int count;

			byte[] input = new byte[DECRYPT_BUFFER_SIZE];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			CipherOutputStream cos = new CipherOutputStream(baos, cipher);
			BufferedInputStream bis = new BufferedInputStream(is);
			while ((count = bis.read(input, 0, DECRYPT_BUFFER_SIZE)) != -1) {
				cos.write(input, 0, count);
			}
			cos.flush();
			cos.close();

			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > INFO Deschifrierung erfolgreich durchgef�hrt");

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(bais));
			ZipEntry entry = zis.getNextEntry();
			if (entry != null) {
				entry.getName();
			}
			BufferedOutputStream dest = new BufferedOutputStream(os);
			byte[] data = new byte[UNZIP_BUFFER_SIZE];
			while ((count = zis.read(data, 0, UNZIP_BUFFER_SIZE)) != -1) {
				dest.write(data, 0, count);
			}

			dest.flush();
			dest.close();
			zis.close();
			baos.close();
			bais.close();

			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > INFO Dearchivierung erfolgreich durchgef�hrt");
		}

		catch (InvalidKeySpecException e) {
			e.printStackTrace(ps);
		} catch (InvalidKeyException e) {
			e.printStackTrace(ps);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace(ps);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace(ps);
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace(ps);
		} catch (IOException e) {
			e.printStackTrace(ps);
		}
	}

	private static String[][] replacements = { { "ä", "&auml" }, { "Ä", "&Auml" }, { "ö", "&ouml" }, { "Ö", "&Ouml" },
			{ "ü", "&uuml" }, { "Ü", "&Uuml" }, { "ß", "&szlig" }, { "§", "c2 a7" }, { "¬", "c2 ac" } };

	public DocumentDetails uploadDocument(String url, String app, StoreDocumentRequest documentDetails,
			String authUsername, String authPassword) {
		if (detailedLogsEnabled) ps.println("Ugur Ausgabe URL: "+url+" App:"+app+" Name:"+authUsername +" Password:"+authPassword);
		DocumentDetails documentResponse = null;
		String serializedObj = null;
		if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " > " + "Entering into addUser method of AddUserClient");
		try {

			ObjectMapper mapper = new ObjectMapper();
			serializedObj = mapper.writeValueAsString(documentDetails);

			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > " + "serializedObj " + serializedObj.length());

			for (String[] replacement : replacements) {
				serializedObj = serializedObj.replaceAll(replacement[0], replacement[1]);
			}
			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > Request " + serializedObj.length());

			HttpEntity<String> requestEntity = new HttpEntity<>(serializedObj,
					AuthenticationHeader.getAuthenticationHeader(authUsername, authPassword));
			RestTemplate templte = new RestTemplate(AuthenticationHeader.httpsRequestFactory());
			templte.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			templte.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

//Das wurde durch Ugur ge�ndert. Die Url soll komplett �bernommen werden anstatt hier erweitert. Sonst ist eine getrennte migration nach Fibu und ZVR nicht m�glich
//			if (detailedLogsEnabled)
//				ps.println(dfOut.format(new Date()) + " > " + "URL :- " + url + "/" + app + "/" + "documents/");
//			ResponseEntity<DocumentDetails> response = templte.exchange(url + "/" + app + "/" + "documents/",
//					HttpMethod.POST, requestEntity, DocumentDetails.class);
			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > " + "URL :- " + url );
			ResponseEntity<DocumentDetails> response = templte.exchange(url,
					HttpMethod.POST, requestEntity, DocumentDetails.class);

//			if (detailedLogsEnabled)
//				ps.println(dfOut.format(new Date()) + " > " + "Got response:- " );
			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > " + "Got response:- "+response );
			documentResponse = response.getBody();
			response = null;

			if (documentResponse.getDocument_access_key() == null) {
				nullResponseReq.add(serializedObj);
			}

			//passedResponsesReq.add(serializedObj);
		} catch (Exception ex) {

			ps.println(dfOut.format(new Date()) + " > " + "UpdateUserClient Exception : " + ex.getMessage());
			//if(detailedLogsEnabled)
			ps.println(ex.getStackTrace());
		//		ps.println(dfOut.format(new Date()) + " > Error in getting response for request object " + serializedObj);
			ex.printStackTrace();
			//failedresponses.add(serializedObj);
			System.gc();

		}
		
		return documentResponse;
	}

	//private List<String> failedresponses = new ArrayList<String>();
	private List<String> passedresponses = new ArrayList<String>();
	//private List<String> passedResponsesReq = new ArrayList<String>();
	private List<String> nullResponseReq = new ArrayList<String>();

	private String uploadDocument(DokumenteVO objDokumenteVO) {

		String accessKey = null;
		String url = sdsRestServerURL;
		String app = sdsRestServerApp;
		String authUsername = sdsRestServerUser;
		String authPassword = sdsRestServerPassword;
		 
		StoreDocumentRequest storeDocumentRequest = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			/** FILE NAME REPLACE WITH BYTE ARRAY OF DOCGEN */
			storeDocumentRequest = new StoreDocumentRequest();
			storeDocumentRequest.setContent(new Content());
			storeDocumentRequest.getContent().setSource(SdsEncoding.generateBase64(objDokumenteVO.getDecryptDataByteArray()));

			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > " + " storeDocumentRequest.getContent().getSource() ->> "
						+" storeDocumentRequest.getContent().getSource()");
			storeDocumentRequest.getContent()
					.setChecksum(SdsEncoding.generateMd5(objDokumenteVO.getDecryptDataByteArray()));

			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > " + " storeDocumentRequest.getContent().getChecksum() ->> "
						+ "storeDocumentRequest.getContent().getChecksum()");

			storeDocumentRequest.setAttributes(new Attributes());
			//Ugur Owner und group einbauen done
			storeDocumentRequest.getAttributes().setOwner(OwnerOfDocument);
			storeDocumentRequest.getAttributes().setGroup(sdsGroup);
			//Ugur DOkumentennamen einbauen done
			storeDocumentRequest.getAttributes().setTitle(objDokumenteVO.getDokName());
			storeDocumentRequest.getAttributes().setEncryption("none");

			if (objDokumenteVO.getCretms() != null) {
				storeDocumentRequest.getAttributes().setCreatedAt(objDokumenteVO.getCretms());

				Calendar calender = Calendar.getInstance();
				calender.setTime(objDokumenteVO.getCretms());
				calender.add(Calendar.YEAR, 5);
				Date validTo = calender.getTime();
				try {
					validTo = new java.sql.Date(df.parse(df.format(validTo)).getTime());
				} catch (Exception e) {
					e.printStackTrace(ps);
				}
				storeDocumentRequest.getAttributes().setValidTo(validTo);
			}

			storeDocumentRequest.getAttributes().setFileFormat("pdf");

			/**
			 * As discussed, now this program has to read data from TMPDOKUMENTE
			 * table, where DOKUMENTART value will be auto populated by talend
			 * program. Hence removing this code.
			 */
			//storeDocumentRequest.getAttributes().setFileName(getDokumentenartForDoktype(objDokumenteVO.getDoktyp()));
			// storeDocumentRequest.getAttributes().setFileName(objDokumenteVO.getDokumentart());
			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > " + objDokumenteVO.getDokName());
			String dokName = objDokumenteVO.getDokName();
		//	ps.println(dfOut.format(new Date()) + " > b4" + dokName);
			for (String[] replacement : replacements) {
				//ps.println(dfOut.format(new Date()) + " > inside if dokname"+dokName );
				dokName = dokName.replaceAll(replacement[0], replacement[1]);
			}
		//	ps.println(dfOut.format(new Date()) + " > after" + dokName);
			storeDocumentRequest.getAttributes().setFileName(dokName);
			
			storeDocumentRequest.getAttributes().setBusinessKey("//templates//docgen//DemoGraphicStateAgeWise.pdf");

			HashMap<String, Object> applicationSpecificData = new HashMap<String, Object>();

			applicationSpecificData = fillValues(objDokumenteVO);

			storeDocumentRequest.getAttributes().setApplicationSpecificData(applicationSpecificData);

			DocumentDetails response = uploadDocument(url, app, storeDocumentRequest, authUsername, authPassword);
			if (response != null) {
				if (detailedLogsEnabled)
					ps.println(dfOut.format(new Date()) + " > " + response.getDocument_access_key());
				accessKey = response.getDocument_access_key();
			}
			applicationSpecificData = null;
			objDokumenteVO = null;
			response = null;
			} catch (Exception e) {
				storeDocumentRequest = null;
				e.printStackTrace(ps);
				e.printStackTrace();
			}
		 catch (Throwable t) {
				storeDocumentRequest = null;
	
				t.printStackTrace();
			}

//		if (detailedLogsEnabled)
//			ps.println(dfOut.format(new Date()) + " > " + " The upload of document having DOKID == "
//					+ objDokumenteVO.getDokid() + " has been DONE. Access key recieved == " + accessKey);
		storeDocumentRequest = null;
		
		return accessKey;

	}

	private HashMap<String, Object> fillValues(DokumenteVO dokumentVO) {

		String[] keys = { "DOKID", "DOKTYPALT", "DOKUMENTENART", "DRUCKTMS", "ALFRESCOKZ", "MELDID", "VMID",
				"ANZSEITEN", "BEVOLLMNR", "DOKBEZ", "LANDKZ" };
		HashMap<String, Object> applicationSpecificData = new HashMap<String, Object>();

		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			switch (key) {
			case "DOKID":
				applicationSpecificData.put("key", "DOKID");
				applicationSpecificData.put("value", dokumentVO.getDokid());
				break;
			case "DOKTYPALT":
				applicationSpecificData.put("key1", "DOKTYPALT");
				applicationSpecificData.put("value1",dokumentVO.getDoktyp() );
				break;
			case "DOKUMENTENART":
				applicationSpecificData.put("key2", "DOKUMENTENART");
				/**
				 * As discussed, now this program has to read data from
				 * TMPDOKUMENTE table, where DOKUMENTART value will be auto
				 * populated by talend program. Hence removing this code.
				 */
				applicationSpecificData.put("value2", dokumentVO.getDokArtbez());
				// applicationSpecificData.put("value2",
				// dokumentVO.getDokumentart());
				break;
			case "DRUCKTMS":
				applicationSpecificData.put("key3", "DRUCKTMS");
				applicationSpecificData.put("value3", dokumentVO.getDrucktms());
				break;
			case "ALFRESCOKZ":
				applicationSpecificData.put("key4", "ALFRESCOKZ");
				applicationSpecificData.put("value4", dokumentVO.getAlfrescokz());
				break;
			case "MELDID":
				applicationSpecificData.put("key5", "MELDID");
				applicationSpecificData.put("value5", dokumentVO.getMeldid());
				break;
			case "VMID":
				applicationSpecificData.put("key6", "VMID");
				applicationSpecificData.put("value6", dokumentVO.getVmid());
				break;
			case "ANZSEITEN":
				applicationSpecificData.put("key7", "ANZSEITEN");
				applicationSpecificData.put("value7", dokumentVO.getAnzseiten());
				break;
			case "BEVOLLMNR":
				applicationSpecificData.put("key8", "BEVOLLMNR");
				applicationSpecificData.put("value8", dokumentVO.getBevollmnr());
				break;
			case "DOKBEZ":
				applicationSpecificData.put("key9", "DOKBEZ");
				applicationSpecificData.put("value9", dokumentVO.getDokbez());
				break;
			case "LANDKZ":
				applicationSpecificData.put("key10", "LANDKZ");
				applicationSpecificData.put("value10", dokumentVO.getLandkz());
				break;
			}

		}

		return applicationSpecificData;
	}
	
	private boolean write(List<TMPDokumenteVO> listMV) {

		Date writeStartTime = new Date();
		Date endTime = null;

		int actualRecordsProcessed = 0;

		try {
			if (listMV == null || listMV.size() <= 0) {

				actualRecordsProcessed = 0;
				return false;
			}

			try (Connection targetConnection = DataSource.getInstance().getTargetConnection();
					PreparedStatement updateSDSIDPreparedStatement = targetConnection
							.prepareStatement(updateSDSIDPreparedStatementSQL);

					Connection sourceConnection = DataSource.getInstance().getSourceConnection();
					PreparedStatement updateSDSIDStatusPreparedStatement = sourceConnection
							.prepareStatement(updateSDSIDStatusPreparedStatementSQL);) {

				writeStartTime = new Date();

				for (TMPDokumenteVO mv : listMV) {
					 if (detailedLogsEnabled)
							ps.println(dfOut.format(new Date()) +rerunable +"<<< Rerunnable >Here called mv.tostring()---"+mv.toString());
					int counter = 1;
					if (mv.getSdsid() != null) {
						 if (detailedLogsEnabled)
								ps.println(dfOut.format(new Date()) +rerunable +"<<< Rerunnable >Here called mv.tostring()-update to sdid--");
						updateSDSIDPreparedStatement.setString(counter++, mv.getSdsid());

//						 if (rerunable)
//						 updateSDSIDPreparedStatement.setBigDecimal(counter++,
//						 new BigDecimal(1));
						
						updateSDSIDPreparedStatement.setString(counter++, mv.getDokid());
						
						updateSDSIDPreparedStatement.addBatch();
						 if (detailedLogsEnabled)
								ps.println(dfOut.format(new Date()) +rerunable +"<<< Rerunnable >Here called mv.tostring()-update batch calledto sdid--");
						actualRecordsProcessed++;
					}

				}

				int[] response = updateSDSIDPreparedStatement.executeBatch();
				StringBuffer sb = new StringBuffer();
				for (int i : response) {
					sb.append(i + ",");
				}

				if (detailedLogsEnabled)
					ps.println(dfOut.format(new Date()) + " > Response of save the blob data : " + sb.toString().length() + " --->actualRecordsProcessed == " + actualRecordsProcessed);
				//
				////////////////////

				if (rerunable) {
					int index = 0;
					for (TMPDokumenteVO mv : listMV) {

						int counter = 1;
						if (response[index] > 0) {
							
							updateSDSIDStatusPreparedStatement.setBigDecimal(counter++, new BigDecimal(1));
							updateSDSIDStatusPreparedStatement.setString(counter++, mv.getDokid());
							updateSDSIDStatusPreparedStatement.addBatch();
						}
					}
					
					int[] response1 = updateSDSIDStatusPreparedStatement.executeBatch();
					StringBuffer sb1 = new StringBuffer();
					for (int i : response1) {
						sb1.append(i + ",");
					}
					if (detailedLogsEnabled)
						ps.println(dfOut.format(new Date()) + " > Response of save status of the blob data : "
								+ sb1.toString().length() + " listMV.size() == " + listMV.size());
				}

				///////////////////
				targetConnection.commit();
				if (rerunable)
					sourceConnection.commit();

			} catch (SQLException e) {
				if (detailedLogsEnabled)
					ps.println(dfOut.format(new Date()) + " > exception called   meld ids "
							+ e.getSQLState());
				e.printStackTrace(ps);

			} catch (PropertyVetoException e) {
				e.printStackTrace(ps);
			}finally {
				try {
					sourceConnection.close();
					targetConnection.close();
				}catch (Exception e) {
					//TODO Ugur: handle exception
				}
				
			}

			if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > INFO  XML-Dokument mit der ID " + listMV.size() + " meld ids "
						+ " wurde erfolgreich in Zieldatenbank generiert (BATCH-INSERT)");
			listMV = null;
		} catch (IOException e) {
			e.printStackTrace(ps);
		} finally {
			endTime = new Date();

			long totalTimeInMillSecFromProcessStart = (endTime.getTime() - writeStartTime.getTime());

			
			
			double ettm = ((double) totalTimeInMillSecFromProcessStart / 1000) / 60;
			setStatisticMap("DBWRITE", ettm);

//			ps.printf(
//					dfOut.format(new Date())
//							+ " > TRP : %d | ETTM : %.3f minutes | ARP : %d | ETAS : %.3f seconds. | RPM : %.3f records/min \n",
//					totalRecProc, ettm, actualRecordsProcessed,
//					(((double) (endTime.getTime() - startTime.getTime()) / 1000)), ((double) totalRecProc / ettm));
		}

		return true;
	}

	

	public boolean startProcessing(int startIndex,int endIndex,int totalMeldCount) {

		boolean recordExist = true;
		
		try {
//				ps.println(dfOut.format(new Date()) + " > INFO  START "
//						+ DokumentProcessor.class.getName());
				doInitialSettings();
				startRecordProcessing();
			
		} finally {
			close();
			//System.out.println(dfOut.format(new Date()) + " > INFO  END DokumenteBLOBDecryptor");
		}
		return recordExist;
	}
//	public void stopReadDBDataExecutor() {
//		try {
//		if(readDBDataExecutor!=null && !readDBDataExecutor.isTerminated() ) 
//			readDBDataExecutor.shutdown();
//		}catch (Exception e) {
//			
//		}
//	}
	private void setStatisticMap(String key,Double value) {
		double valueCalculated =0.0;
		try {
			if(statisticMap.containsKey(key)) {
				valueCalculated = statisticMap.get(key);
				valueCalculated = valueCalculated+value;
			}else {
				valueCalculated = value;
			}
//			ps.println(dfOut.format(new Date()) + " > key   :" + key + " value-- "+value
//					+ " valueCalculated ---"+valueCalculated);
			statisticMap.put(key, valueCalculated);
		}catch(Exception e) {
			statisticMap.put(key, value);
		}
	}
	public HashMap<String,Double> getStatisticMap() {
		return statisticMap;
	}
	public void initializeStatisticMap() {
		statisticMap.put("DBREAD", 0.0);
		statisticMap.put("RECPROCSTIME", 0.0);
		statisticMap.put("SDSTIME", 0.0);
		statisticMap.put("DBWRITE", 0.0);
		ps.println("statMap-b4 start process---"+statisticMap);
	}
}
