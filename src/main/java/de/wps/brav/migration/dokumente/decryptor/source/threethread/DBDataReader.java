package de.wps.brav.migration.dokumente.decryptor.source.threethread;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import de.wps.brav.migration.dokumente.db.DataSource;
import de.wps.brav.migration.dokumente.db.DokumenteVO;

public class DBDataReader implements Callable<List<DokumenteVO>> {
	private static DateFormat dfOut = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private static boolean detailedLogsEnabled = false;
	private static String[][] replacements = { { "ä", "ae" }, { "Ä", "ae" }, { "ö", "oe" }, { "Ö", "Oe" },
			{ "ü", "ue" }, { "Ü", "Ue" }, { "ß", "ss" }, { "§", "ue" }, { "¬", "ue" } };
	int startIndex ;
	int lastIndex;
	String selectCryptDataAndIdPagingQuery;
	boolean rerunable;PrintStream ps;
	public DBDataReader(int offset, int range,String selectCryptDataAndIdPagingQuery, boolean rerunable,PrintStream ps) {
		this.startIndex = offset;
		this.lastIndex = range;
		this.selectCryptDataAndIdPagingQuery = selectCryptDataAndIdPagingQuery;
		this.rerunable = rerunable;
		this.ps = ps;
	}
	//public static List<DokumenteVO> read(int offset, int range,String selectCryptDataAndIdPagingQuery, boolean rerunable,PrintStream ps) {
	 public List<DokumenteVO>  call() {
//		if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " class DBDataReader method read in "
					+ "> Reading data from offset == " + startIndex + " lastIndex == " + lastIndex);
		
		List<DokumenteVO> listDokumenteVO = new ArrayList<DokumenteVO>();
		listDokumenteVO.clear();
		Connection sourceConnection = null;
		try  {
			 sourceConnection = DataSource.getInstance().getSourceConnection();
			 PreparedStatement statement = sourceConnection.prepareStatement(selectCryptDataAndIdPagingQuery);
			
			statement.setInt(1, lastIndex);
			statement.setInt(2, startIndex);
			ResultSet rs = statement.executeQuery();
			statement.setFetchSize(lastIndex-startIndex);
			while (rs.next()) {
				String dokid = rs.getString("DOKID");
				BigDecimal doktyp = rs.getBigDecimal("DOKTYP");
				Date cretms = rs.getDate("CRETMS");
				Date drucktms = rs.getDate("DRUCKTMS");
				boolean alfrescokz = rs.getBoolean("ALFRESCOKZ");
				Date gelesentms = rs.getDate("GELESENTMS");
				String meldid = rs.getString("MELDID");
				String vmid = rs.getString("VMID");
				BigDecimal anzseiten = rs.getBigDecimal("ANZSEITEN");
				BigDecimal bevollmnr = rs.getBigDecimal("BEVOLLMNR");
				String dokbez = rs.getString("DOKBEZ");
				Blob dokdata = rs.getBlob("DOKDATA");
				String landkz = rs.getString("LANDKZ");
				boolean status = false;
				if (rerunable) {
					status = rs.getBoolean("STATUS");
				}
				//String dokumentart = rs.getString("DOKUMENTART");
				String dokumentName = rs.getString("DOKNAME");
				String dokArtbez = rs.getString("DOKUMENTARTBEZ");
				dokumentName =dokumentName != null ? dokumentName : "-";

				DokumenteVO objDokumenteVO = new DokumenteVO(dokid, doktyp, cretms, drucktms, alfrescokz, gelesentms,
						meldid, vmid, anzseiten, bevollmnr, dokbez, dokdata, landkz);
				objDokumenteVO.setDokArtbez(dokArtbez);
				objDokumenteVO.setStatus(status);
				//objDokumenteVO.setDokumentart((dokumentart != null) ? dokumentart : "-");
//				for (String[] replacement : replacements) {
//					dokumentName = dokumentName.replace(replacement[0], replacement[1]);
//				}

				dokumentName = dokumentName.replaceAll("[$&+,:;=?@#|/'\\\\<>^*()%]", "-");
				objDokumenteVO.setDokName(dokumentName);
//				if (detailedLogsEnabled)
//					ps.println(dfOut.format(new Date()) + " > Reading data from offdokumentNameset == " + dokumentName );

				byte[] blobByteArray = null;
				if (dokdata != null) {
					blobByteArray = dokdata.getBytes((long) 1, (int) (dokdata.length()));
				}

				objDokumenteVO.setCryptDataByteArray(blobByteArray);

				listDokumenteVO.add(objDokumenteVO);

			}
		//	if (detailedLogsEnabled)
				ps.println(dfOut.format(new Date()) + " > Reading data from offset == " + startIndex + " range == " + lastIndex
						+ "Total records found :- " + listDokumenteVO.size());

		} catch (SQLException e) {
			e.printStackTrace(ps);
		} catch (IOException e) {
			e.printStackTrace(ps);
		} catch (PropertyVetoException e) {
			e.printStackTrace(ps);
		}finally {
			try {
				sourceConnection.close();
			}catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		if (detailedLogsEnabled)
			ps.println(dfOut.format(new Date()) + " >Data frim dbreader sent to call function "  + listDokumenteVO.size());
		return listDokumenteVO;
	}


}
